package com.nycschools.jvr.a20190914_joserodriguez_nycschools;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.data.Repository;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.data.models.SatScores;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.data.models.SchoolData;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.utils.SchedulerProvider;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.utils.SchoolUtil;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.viewmodels.SchoolDetailsViewModel;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.viewmodels.SchoolsViewModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Scanner;

import io.reactivex.Observable;
import io.reactivex.schedulers.TestScheduler;

public class ViewModelUnitTest {
    @Rule
    public TestRule rule = new InstantTaskExecutorRule();
    @Mock
    private Repository repository;
    @Mock
    private SchedulerProvider schedulerProvider;
    @InjectMocks
    private SchoolsViewModel schoolsViewModel;
    @InjectMocks
    private SchoolDetailsViewModel schoolDetailsViewModel;
    private TestScheduler testScheduler;
    private List<SchoolData> schoolDataMock;
    private List<SatScores> satScoresMock;


    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        InputStream schoolsStream = getClass().getResourceAsStream("/schools.json");
        InputStream satStream = getClass().getResourceAsStream("/satscores.json");
        String schoolsJson = convertStreamToString(schoolsStream);
        String satJson = convertStreamToString(satStream);

        Type collectionType = new TypeToken<List<SchoolData>>(){}.getType();
        Type collectionType2 = new TypeToken<List<SatScores>>(){}.getType();
        schoolDataMock = new Gson().fromJson(schoolsJson, collectionType);
        satScoresMock = new Gson().fromJson(satJson, collectionType2);

        testScheduler = new TestScheduler();
        schoolsViewModel = new SchoolsViewModel(schedulerProvider, repository);
        schoolDetailsViewModel = new SchoolDetailsViewModel(schedulerProvider, repository);

    }

    public String convertStreamToString(InputStream stream){
        Scanner scanner = new Scanner(stream).useDelimiter("\\A");
        return scanner.hasNext() ? scanner.next() : "";
    }

    @Test
    public void getSchoolDataTestSuccess(){
        Mockito.when(schedulerProvider.io()).thenReturn(testScheduler);
        Mockito.when(schedulerProvider.ui()).thenReturn(testScheduler);
        Mockito.when(repository.getSchoolData()).thenReturn(Observable.just(schoolDataMock));

        schoolsViewModel.makeSchoolDataCall();
        testScheduler.triggerActions();

        Assert.assertFalse(schoolsViewModel.getIsLoaderVisible().getValue());
        Assert.assertFalse(schoolsViewModel.getIsError().getValue());
        Assert.assertEquals(schoolDataMock.size(), schoolsViewModel.getLiveSchoolData().getValue().size());
        Assert.assertEquals(schoolDataMock.get(0).getDbn(), schoolsViewModel.getLiveSchoolData().getValue().get(0).getDbn());
        Assert.assertEquals(schoolDataMock.get(0).getSchoolName(), schoolsViewModel.getLiveSchoolData().getValue().get(0).getSchoolName());

        SchoolData data = schoolsViewModel.getLiveSchoolData().getValue().get(0);
        Assert.assertEquals("Grades: 6-12", SchoolUtil.getDisplayGrades(data.getFinalgrades()));
        Assert.assertEquals("10 East 15th Street, Manhattan NY 10003", SchoolUtil.getLocationAddress(data.getLocation()));


    }

    @Test
    public void getSchoolDataTestError(){
        Throwable t = new Throwable();
        Mockito.when(schedulerProvider.io()).thenReturn(testScheduler);
        Mockito.when(schedulerProvider.ui()).thenReturn(testScheduler);
        Mockito.when(repository.getSchoolData()).thenReturn(Observable.error(t));

        schoolsViewModel.makeSchoolDataCall();
        testScheduler.triggerActions();

        Assert.assertFalse(schoolsViewModel.getIsLoaderVisible().getValue());
        Assert.assertTrue(schoolsViewModel.getIsError().getValue());
        Assert.assertEquals(null, schoolsViewModel.getLiveSchoolData().getValue());


    }

    @Test
    public void getSatDetailsTestSuccess(){
        Mockito.when(schedulerProvider.io()).thenReturn(testScheduler);
        Mockito.when(schedulerProvider.ui()).thenReturn(testScheduler);
        Mockito.when(repository.getSatData(schoolDataMock.get(139).getDbn())).thenReturn(Observable.just(satScoresMock));

        schoolDetailsViewModel.initViewModel(schoolDataMock.get(139));
        schoolDetailsViewModel.makeSatScoresCall();
        testScheduler.triggerActions();

        Assert.assertFalse(schoolDetailsViewModel.getIsLoaderVisible().getValue());
        Assert.assertFalse(schoolDetailsViewModel.getIsError().getValue());
        Assert.assertEquals(schoolDataMock.get(139).getDbn(), schoolDetailsViewModel.getLiveSatScores().getValue().getDbn());
        Assert.assertEquals("404", schoolDetailsViewModel.getLiveSatScores().getValue().getSatMathAvgScore());
        Assert.assertEquals("355", schoolDetailsViewModel.getLiveSatScores().getValue().getSatCriticalReadingAvgScore());
        Assert.assertEquals("363", schoolDetailsViewModel.getLiveSatScores().getValue().getSatWritingAvgScore());
        Assert.assertEquals("mdoyle9@schools.nyc.gov", schoolDetailsViewModel.getLiveSchoolData().getValue().getSchoolEmail());
        Assert.assertEquals("212-406-9411", schoolDetailsViewModel.getLiveSchoolData().getValue().getPhoneNumber());
        Assert.assertEquals("http://schools.nyc.gov/SchoolPortals/01/M292/default.htm", schoolDetailsViewModel.getLiveSchoolData().getValue().getWebsite());


    }

    @Test
    public void getSatDetailsTestError(){
        Throwable t = new Throwable();
        Mockito.when(schedulerProvider.io()).thenReturn(testScheduler);
        Mockito.when(schedulerProvider.ui()).thenReturn(testScheduler);
        Mockito.when(repository.getSatData(schoolDataMock.get(139).getDbn())).thenReturn(Observable.error(t));

        schoolDetailsViewModel.initViewModel(schoolDataMock.get(139));
        schoolDetailsViewModel.makeSatScoresCall();
        testScheduler.triggerActions();

        Assert.assertFalse(schoolDetailsViewModel.getIsLoaderVisible().getValue());
        Assert.assertTrue(schoolDetailsViewModel.getIsError().getValue());
        Assert.assertEquals(null, schoolDetailsViewModel.getLiveSatScores().getValue());


    }

}
