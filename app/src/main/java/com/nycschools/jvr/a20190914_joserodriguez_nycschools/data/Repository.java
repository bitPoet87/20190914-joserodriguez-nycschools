package com.nycschools.jvr.a20190914_joserodriguez_nycschools.data;

import com.nycschools.jvr.a20190914_joserodriguez_nycschools.data.models.SatScores;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.data.models.SchoolData;

import java.util.List;

import io.reactivex.Observable;

public interface Repository {

    Observable<List<SchoolData>> getSchoolData();
    Observable<List<SchoolData>> getSchoolDataByBoro(String boro);
    Observable<List<SatScores>> getSatData(String dbn);
}
