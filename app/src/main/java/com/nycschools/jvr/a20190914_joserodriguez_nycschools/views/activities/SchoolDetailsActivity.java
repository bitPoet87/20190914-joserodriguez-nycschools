package com.nycschools.jvr.a20190914_joserodriguez_nycschools.views.activities;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.R;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.data.models.SchoolData;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.databinding.ActivitySchoolDetailsBinding;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.viewmodels.SchoolDetailsViewModel;

public class SchoolDetailsActivity extends BaseActivity implements OnMapReadyCallback {
    private ActivitySchoolDetailsBinding binding;
    private SchoolDetailsViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_school_details);
        viewModel = ViewModelProviders.of(this, factory).get(SchoolDetailsViewModel.class);
        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);
        viewModel.initViewModel(getIntent().getParcelableExtra("data"));
        viewModel.makeSatScoresCall();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        addToolbar();
        observeViewModel();
    }

    private void addToolbar(){
        setSupportActionBar(binding.myToolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void observeViewModel(){
        viewModel.getIsError().observe(this,errorObserver());

    }

    private Observer<Boolean> errorObserver(){
        return  isError -> {
            if (isError){
                generalError(binding.getRoot(), getString(R.string.sat_error_msg));
            }

        };
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        SchoolData schoolData = viewModel.getLiveSchoolData().getValue();
        LatLng school = new LatLng(Double.parseDouble(schoolData.getLatitude()), Double.parseDouble(schoolData.getLongitude()));
        googleMap.addMarker(new MarkerOptions().position(school).title(schoolData.getSchoolName()));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(school, 16.0f));

    }
}
