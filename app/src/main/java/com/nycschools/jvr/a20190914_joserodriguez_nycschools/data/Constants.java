package com.nycschools.jvr.a20190914_joserodriguez_nycschools.data;

public class Constants {

    public static final String MANHATTAN = "M";
    public static final String QUEENS = "Q";
    public static final String BROOKLYN = "K";
    public static final String BRONX = "X";
    public static final String STATEN_ISLAND = "R";
}
