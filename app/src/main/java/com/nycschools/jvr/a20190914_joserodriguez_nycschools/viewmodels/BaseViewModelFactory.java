package com.nycschools.jvr.a20190914_joserodriguez_nycschools.viewmodels;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.nycschools.jvr.a20190914_joserodriguez_nycschools.data.Repository;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.utils.SchedulerProvider;

public class BaseViewModelFactory implements ViewModelProvider.Factory {
    private Repository repository;
    private SchedulerProvider provider;

    public BaseViewModelFactory(SchedulerProvider provider, Repository repository){
        this.provider = provider;
        this.repository = repository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(SchoolsViewModel.class)){
            return (T) new SchoolsViewModel(provider, repository);
        }else if (modelClass.isAssignableFrom(SchoolDetailsViewModel.class)){
            return (T) new SchoolDetailsViewModel(provider, repository);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");

    }
}
