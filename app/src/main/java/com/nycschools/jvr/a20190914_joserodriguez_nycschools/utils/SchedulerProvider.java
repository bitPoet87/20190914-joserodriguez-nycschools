package com.nycschools.jvr.a20190914_joserodriguez_nycschools.utils;

import io.reactivex.Scheduler;

public interface SchedulerProvider {
    Scheduler ui();
    Scheduler computation();
    Scheduler io();
}
