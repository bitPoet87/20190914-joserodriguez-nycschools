package com.nycschools.jvr.a20190914_joserodriguez_nycschools.viewmodels;

import androidx.lifecycle.ViewModel;

import com.nycschools.jvr.a20190914_joserodriguez_nycschools.data.Repository;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.utils.SchedulerProvider;

import io.reactivex.disposables.Disposable;

public class BaseViewModel extends ViewModel {
    protected Repository repository;
    protected SchedulerProvider provider;
    protected Disposable subscription;

    public BaseViewModel(SchedulerProvider provider, Repository repository){
        this.provider = provider;
        this.repository = repository;
    }

    @Override
    protected void onCleared() {
        if (subscription != null){
            subscription.dispose();
        }
        super.onCleared();
    }
}
