package com.nycschools.jvr.a20190914_joserodriguez_nycschools.viewmodels;

import androidx.lifecycle.MutableLiveData;

import com.nycschools.jvr.a20190914_joserodriguez_nycschools.data.models.SatScores;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.data.Repository;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.data.models.SchoolData;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.utils.SchedulerProvider;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.utils.SchoolUtil;

import java.util.List;

public class SchoolDetailsViewModel extends BaseViewModel {
    private static final String TAG = SchoolDetailsViewModel.class.getSimpleName();
    private MutableLiveData<Boolean> isLoaderVisible;
    private MutableLiveData<Boolean> isError;
    private MutableLiveData<SchoolData> liveSchoolData;
    private MutableLiveData<SatScores> liveSatScores;

    public MutableLiveData<Boolean> getIsLoaderVisible() {
        return isLoaderVisible;
    }

    public MutableLiveData<Boolean> getIsError() {
        return isError;
    }

    public MutableLiveData<SchoolData> getLiveSchoolData() {
        return liveSchoolData;
    }

    public MutableLiveData<SatScores> getLiveSatScores() {
        return liveSatScores;
    }

    public SchoolDetailsViewModel(SchedulerProvider provider, Repository repository) {
        super(provider, repository);
    }

    public void initViewModel(SchoolData data){
        liveSatScores   = new MutableLiveData<>();
        isLoaderVisible = new MutableLiveData<>();
        isError         = new MutableLiveData<>();
        liveSchoolData  = new MutableLiveData<>();

        data.setOverviewParagraph(SchoolUtil.parseOverview(data.getOverviewParagraph()));
        liveSchoolData.setValue(data);

    }

    public void makeSatScoresCall(){
        isLoaderVisible.setValue(true);

        subscription = repository.getSatData(liveSchoolData.getValue().getDbn())
                .subscribeOn(provider.io())
                .observeOn(provider.ui())
                .subscribe(result -> retrieveScoresSuccess(result),
                        throwable -> retrieveScoresError(throwable) );

    }

    private void retrieveScoresSuccess(List<SatScores> satScores){
        isLoaderVisible.setValue(false);
        isError.setValue(false);
        liveSatScores.setValue(satScores.get(0));
    }

    private void retrieveScoresError(Throwable throwable){
        isLoaderVisible.setValue(false);
        isError.setValue(true);
    }

}
