package com.nycschools.jvr.a20190914_joserodriguez_nycschools.utils;

import android.util.Log;

public class SchoolUtil {
    private static final String TAG = SchoolUtil.class.getSimpleName();

    public static String getLocationAddress(String location){
        try{
            String[] locationArray = location.split("\\(");
            return locationArray[0].trim();
        }catch (Exception e){
            Log.d(TAG, e.getMessage());
            return "";
        }
    }

    public static String getDisplayGrades(String grades){
        try {
            if (grades.contains("Grades:")){
                return grades;
            }else{
                return "Grades: " + grades;
            }

        }catch (Exception e){
            Log.d(TAG, e.getMessage());
            return "";
        }
    }

    public static String parseOverview(String overview){
        return overview.replace("Â", "");
    }
}
