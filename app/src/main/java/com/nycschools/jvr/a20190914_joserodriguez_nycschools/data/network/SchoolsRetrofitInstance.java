package com.nycschools.jvr.a20190914_joserodriguez_nycschools.data.network;

import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class SchoolsRetrofitInstance {

    private static Retrofit retrofit;
    private static final String BASE_URL = "https://data.cityofnewyork.us/";
    private static HttpLoggingInterceptor interceptor;
    private static OkHttpClient client;


    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            initClient();

            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                    .client(client)
                    .build();
        }
        return retrofit;
    }

    private static void initClient(){
        interceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC);
        client = new OkHttpClient().newBuilder().addInterceptor(interceptor).build();
    }


}
