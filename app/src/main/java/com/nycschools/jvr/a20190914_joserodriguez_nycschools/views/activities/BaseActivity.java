package com.nycschools.jvr.a20190914_joserodriguez_nycschools.views.activities;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.R;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.data.SchoolsRepository;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.utils.AppSchedulerProvider;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.viewmodels.BaseViewModelFactory;


public class BaseActivity extends AppCompatActivity {
    protected BaseViewModelFactory factory;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        factory = new BaseViewModelFactory(new AppSchedulerProvider(), new SchoolsRepository());
    }

    protected void generalError(View view, String error){
        Snackbar.make(view, error, Snackbar.LENGTH_LONG).show();
    }
}
