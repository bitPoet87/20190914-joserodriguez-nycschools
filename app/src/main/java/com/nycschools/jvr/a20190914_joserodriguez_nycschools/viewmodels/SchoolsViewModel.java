package com.nycschools.jvr.a20190914_joserodriguez_nycschools.viewmodels;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.data.models.SchoolData;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.data.Repository;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.utils.SchedulerProvider;
import java.util.List;

public class SchoolsViewModel extends BaseViewModel {
    private final static String TAG = SchoolsViewModel.class.getSimpleName();
    private MutableLiveData<List<SchoolData>> liveSchoolData;
    private MutableLiveData<Boolean> isLoaderVisible;
    private MutableLiveData<Boolean> isError;

    public MutableLiveData<List<SchoolData>> getLiveSchoolData() {
        return liveSchoolData;
    }

    public MutableLiveData<Boolean> getIsLoaderVisible() {
        return isLoaderVisible;
    }

    public MutableLiveData<Boolean> getIsError() {
        return isError;
    }

    public SchoolsViewModel(SchedulerProvider provider, Repository repository){
        super(provider, repository);

        initLiveData();
    }

    private void initLiveData(){
        liveSchoolData  = new MutableLiveData<>();
        isLoaderVisible = new MutableLiveData<>();
        isError         = new MutableLiveData<>();
    }

    public void makeSchoolDataCall(){
        isLoaderVisible.setValue(true);

        subscription = repository.getSchoolData()
                                 .subscribeOn(provider.io())
                                 .observeOn(provider.ui())
                                 .subscribe(result -> retrieveSchoolsSuccess(result),
                                         throwable -> retrieveSchoolsError(throwable) );

    }

    public void makeSchoolbyBoroCall(String boro){
        isLoaderVisible.setValue(true);

        subscription = repository.getSchoolDataByBoro(boro)
                .subscribeOn(provider.io())
                .observeOn(provider.ui())
                .subscribe(result -> retrieveSchoolsSuccess(result),
                        throwable -> retrieveSchoolsError(throwable) );

    }


    private void retrieveSchoolsSuccess(List<SchoolData> schoolData){
        isLoaderVisible.setValue(false);
        isError.setValue(false);
        liveSchoolData.setValue(schoolData);

    }

    private void retrieveSchoolsError(Throwable throwable){
        isLoaderVisible.setValue(false);
        isError.setValue(true);


    }

}
