package com.nycschools.jvr.a20190914_joserodriguez_nycschools.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.nycschools.jvr.a20190914_joserodriguez_nycschools.R;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.data.models.SchoolData;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.databinding.SchoolRowItemBinding;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.utils.SchoolUtil;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.viewmodels.SchoolsViewModel;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.views.activities.SchoolDetailsActivity;

public class SchoolsListAdapter extends RecyclerView.Adapter<SchoolsListAdapter.SchoolsListViewHolder>{
    private Context context;
    private SchoolsViewModel viewModel;

    public SchoolsListAdapter(Context context, SchoolsViewModel viewModel){
        this.context = context;
        this.viewModel = viewModel;
    }

    @NonNull
    @Override
    public SchoolsListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        SchoolRowItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.school_row_item, parent, false);

        return new SchoolsListViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull SchoolsListViewHolder holder, int position) {
        SchoolData item = viewModel.getLiveSchoolData().getValue().get(position);
        item.setLocation(SchoolUtil.getLocationAddress(item.getLocation()));
        item.setFinalgrades(SchoolUtil.getDisplayGrades(item.getFinalgrades()));
        holder.bind(item);

    }

    @Override
    public int getItemCount() {
        return viewModel.getLiveSchoolData().getValue() != null ? viewModel.getLiveSchoolData().getValue().size() : 0;
    }


    public class SchoolsListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        SchoolRowItemBinding binding;

        public SchoolsListViewHolder(SchoolRowItemBinding binding){
            super(binding.getRoot());
            this.binding = binding;
            this.binding.getRoot().setOnClickListener(this);
        }

        public void bind(SchoolData data){
            binding.setSchoolData(data);
            binding.executePendingBindings();

        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, SchoolDetailsActivity.class);
            intent.putExtra("data", viewModel.getLiveSchoolData().getValue().get(getAdapterPosition()));
            context.startActivity(intent);

        }
    }

}
