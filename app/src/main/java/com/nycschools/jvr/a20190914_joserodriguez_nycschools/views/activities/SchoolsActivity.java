package com.nycschools.jvr.a20190914_joserodriguez_nycschools.views.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.R;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.data.Constants;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.data.models.SchoolData;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.databinding.ActivitySchoolsBinding;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.viewmodels.SchoolsViewModel;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.views.adapters.SchoolsListAdapter;

import java.util.List;

public class SchoolsActivity extends BaseActivity {
    private SchoolsViewModel viewModel;
    private ActivitySchoolsBinding binding;
    private SchoolsListAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_schools);

        viewModel = ViewModelProviders.of(this, factory).get(SchoolsViewModel.class);
        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);
        viewModel.makeSchoolDataCall();
        addToolbar();
        addRecyclerView();
        observeViewModel();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.boro_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.bronx:
                viewModel.makeSchoolbyBoroCall(Constants.BRONX);
                return true;
            case R.id.brooklyn:
                viewModel.makeSchoolbyBoroCall(Constants.BROOKLYN);
                return true;
            case R.id.manhattan:
                viewModel.makeSchoolbyBoroCall(Constants.MANHATTAN);
                return true;
            case R.id.queens:
                viewModel.makeSchoolbyBoroCall(Constants.QUEENS);
                return true;
            case R.id.statenisland:
                viewModel.makeSchoolbyBoroCall(Constants.STATEN_ISLAND);
                return true;

            default:
                viewModel.makeSchoolDataCall();
                return true;
        }
    }

    private void addToolbar(){
        setSupportActionBar(binding.myToolbar);
        getSupportActionBar().setTitle(null);
    }

    private void addRecyclerView(){
        adapter = new SchoolsListAdapter(this, viewModel);
        binding.schoolsRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        binding.schoolsRv.setAdapter(adapter);

    }

    private void observeViewModel(){
        viewModel.getLiveSchoolData().observe(this, schoolDataObserver());
        viewModel.getIsError().observe(this, errorObserver());
    }

    private Observer<List<SchoolData>> schoolDataObserver(){
        return schoolData -> {
            if (schoolData != null){
                adapter.notifyDataSetChanged();
            }
        };
    }

    private Observer<Boolean> errorObserver(){
        return  isError -> {
            if (isError){
                generalError(binding.getRoot(), getString(R.string.api_error_msg));
            }

        };
    }
}
