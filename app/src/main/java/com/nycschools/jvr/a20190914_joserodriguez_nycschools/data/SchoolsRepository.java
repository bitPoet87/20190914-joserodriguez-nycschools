package com.nycschools.jvr.a20190914_joserodriguez_nycschools.data;

import com.nycschools.jvr.a20190914_joserodriguez_nycschools.data.models.SatScores;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.data.models.SchoolData;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.data.network.SchoolsApi;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.data.network.SchoolsRetrofitInstance;

import java.util.List;

import io.reactivex.Observable;


public class SchoolsRepository implements Repository {
    private SchoolsApi api;

    public SchoolsRepository(){
        this.api = SchoolsRetrofitInstance.getRetrofitInstance().create(SchoolsApi.class);
    }

    public Observable<List<SchoolData>> getSchoolData(){
        return api.getSchoolList();
    }

    public Observable<List<SchoolData>> getSchoolDataByBoro(String boro){
        return api.getSchoolsbyBoro(boro);
    }

    public Observable<List<SatScores>> getSatData(String dbn){
        return api.getSatScores(dbn);
    }

}
