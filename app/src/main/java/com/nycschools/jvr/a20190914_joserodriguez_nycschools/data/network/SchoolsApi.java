package com.nycschools.jvr.a20190914_joserodriguez_nycschools.data.network;

import com.nycschools.jvr.a20190914_joserodriguez_nycschools.data.models.SatScores;
import com.nycschools.jvr.a20190914_joserodriguez_nycschools.data.models.SchoolData;


import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SchoolsApi {

    @GET("resource/s3k6-pzi2.json")
    Observable<List<SchoolData>> getSchoolList();

    @GET("resource/s3k6-pzi2.json")
    Observable<List<SchoolData>> getSchoolsbyBoro(@Query("boro") String boro);

    @GET("resource/f9bf-2cp4.json")
    Observable<List<SatScores>> getSatScores(@Query("dbn") String dbn);

}
